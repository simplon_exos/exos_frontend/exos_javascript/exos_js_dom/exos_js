let para2 = document.querySelector("#para2");
para2.style.color = '#0000FF';

document.querySelector("#section2").style.border = "1px dashed #000000";

document.querySelector("#section2 .colorful").style.backgroundColor = "orange";

document.querySelector("#section1 h2").style.fontStyle = "italic";

document.querySelector("p .colorful").style.display = "none";
// on peut utiliser .style.visibility = 'hidden' mais l'élément gardera sa place ds le DOM, alors qu'avec display = none; il disparait de la structure de la page.

para2.textContent = "Modified by JS";
// on peut utiliser .innerHTML, mais on aura en retour le code HTML avec les balises. Alors que textContent renvoie uniquement le texte contenu dans les balises.


document.querySelector("#section1 a").href = "http://www.simplonlyon.fr";

document.querySelector("#section2 h2").classList.add("big-text");
// h2Sect2.className += ' big-text"; // moins bien à utiliser

let paragraph = document.querySelectorAll("p");
for (const item of paragraph){
    item.style.fontStyle = "italic";
}

//for(let i = 0 ; i < paragraph.length ; i++){ paragraph[i].dtyle.fontStyle = 'italic'}