let firstBtn = document.querySelector('button');

// firstBtn.addEventListener('click', test);
//firstBtn.addEventListener('click', test()); NE FONCTIONNERA PAS CAR LA FONCTION EST APPELEE AVEC LES () --> renverra UNDEFINED

// function test(){
//     console.log("bloup !");
// }

// La syntaxe la plus courante est de définir la fonction dans l'eventAddListener (fonction anonyme) car on ne s'en resert pas en dehors de lui:

firstBtn.addEventListener('click', function() {
    console.log('bloup !');
});