let count = 0;

let plus = document.querySelector('#btnPlus');
let minus = document.querySelector('#btnMinus');
let counterHTML = document.querySelector('span');
let reset = document.querySelector('#btnReset')

displayCount();

// plus.addEventListener('click', function(){
//     count++;
//     displayCount();
// });

// minus.addEventListener('click', function(){
//     count--;
//     displayCount();
// });


// reset.addEventListener('click', function(){
//     count = 0;
//     displayCount();
// });


// function displayCount() {
//     counterHTML.textContent = count;
// }
//BONUS: Refactoriser tout ça !!

let counterBtnList = document.querySelectorAll('button');
for(let btn of counterBtnList){
    btn.addEventListener('click', function(){
        if(btn.id === 'btnPlus'){
            count++;
        }
        if(btn.id === 'btnMinus'){
            count--;
        }
        if(btn.id === 'btnReset'){
            count = 0;
        }
        displayCount();
    });
}

function displayCount() {
    counterHTML.textContent = count;
}